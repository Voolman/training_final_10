import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(String name, String email, String password) async {
  return supabase.auth.signUp(email: email, password: password, data: {});
}

Future<void> dataSignUp(String phone, String fullName) async {
  var currentId = getCurrentUserId()!;
  return supabase
      .from('profiles')
      .insert({'id_user': currentId, 'fullname': fullName, 'phone': phone});
}

Future<AuthResponse> signIn(String email, String password) async {
  return supabase.auth.signInWithPassword(email: email, password: password);
}

Future<void> signOut() async {
  return supabase.auth.signOut();
}

Future<void> sendOTP(String email) async {
  return supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(String email, String code) {
  return supabase.auth
      .verifyOTP(email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) {
  return supabase.auth.updateUser(UserAttributes(password: password));
}

String? getCurrentUserId() {
  var res = supabase.auth.currentUser?.id;
  return res;
}
