import 'package:flutter/material.dart';

void showError(BuildContext context, String e){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: const Text('Ошибка'),
    content: Text(e),
    actions: [
      TextButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          child: const Text("OK")
      )
    ],
  )
  );
}

void showLoading(BuildContext context){
  showDialog(context: context, builder: (_) => const PopScope(
      canPop: false,
      child: Dialog(
        backgroundColor: Colors.transparent,
        surfaceTintColor: Colors.transparent,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      )
  )
  );
}
