import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_10/presentation/theme/colors.dart';

class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final bool enableObscure;
  final TextEditingController controller;
  final Function(String) onChanged;
  final bool isValid;
  const CustomTextField({super.key, required this.label, required this.hint, this.enableObscure = false, required this.controller, required this.onChanged, required this.isValid});


  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}
bool isObscure = true;
var colors = LightColorsApp();
class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.labelLarge,
        ),
        SizedBox(height: 12),
        SizedBox(
          width: double.infinity,
          height: 48,
          child: TextField(
            controller: widget.controller,
            obscureText: (widget.enableObscure) ? isObscure : false,
            obscuringCharacter: '•',
            onChanged: widget.onChanged,
            decoration: InputDecoration(
              fillColor: colors.lightGray,
              filled: true,
              focusedBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.focusedBorder : Theme.of(context).inputDecorationTheme.errorBorder,
              enabledBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.enabledBorder : Theme.of(context).inputDecorationTheme.errorBorder,
              contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 14),
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleSmall,
              suffixIcon: (widget.enableObscure) ? GestureDetector(
                onTap: (){
                  setState(() {
                    isObscure = !isObscure;
                  });
                },
                child: SvgPicture.asset('assets/eye.svg', fit: BoxFit.scaleDown,),
              ) : null
            ),
          ),
        )
      ],
    );
  }
}