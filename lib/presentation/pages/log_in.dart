import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:training_final_10/presentation/pages/forgot_password.dart';
import 'package:training_final_10/presentation/pages/sign_up.dart';
import 'package:training_final_10/presentation/theme/colors.dart';
import 'package:training_final_10/presentation/widgets/CustomTextField.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              const SizedBox(height: 66),
              Row(
                children: [
                  Container(
                      height: 44,
                      width: 44,
                      decoration: BoxDecoration(
                          color: colors.lightGray,
                          borderRadius: BorderRadius.circular(360)),
                      child: SvgPicture.asset('assets/back_arrow.svg',
                          fit: BoxFit.scaleDown)),
                ],
              ),
              const SizedBox(height: 11),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Привет!',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 8),
                  Text(
                      'Заполните Свои Данные Или\n Продолжите Через Социальные Медиа',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleMedium)
                ],
              ),
              const SizedBox(height: 30),
              CustomTextField(
                  label: 'Email',
                  hint: 'xyz@gmail.com',
                  controller: email,
                  onChanged: onChanged,
                  isValid: true
              ),
              const SizedBox(height: 30),
              CustomTextField(
                  label: 'Пароль',
                  hint: '••••••••',
                  controller: password,
                  enableObscure: true,
                  onChanged: onChanged,
                  isValid: true
              ),
              const SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ForgotPassword()));
                    },
                    child: Text(
                      'Востановить',
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: colors.gray,
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              height: 4/3
                          )
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(height: 24),
              SizedBox(
                width: double.infinity,
                height: 50,
                child: FilledButton(
                    onPressed: (){},
                    style: Theme.of(context).filledButtonTheme.style,
                    child: const Text('Войти')
                ),
              ),
              const SizedBox(height: 209),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SignUp()));
                },
                child: RichText(text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'Вы впервые? ',
                        style: GoogleFonts.raleway(
                            textStyle: TextStyle(
                                color: colors.gray,
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                height: 18.78/16
                            )
                        ),
                      ),
                      TextSpan(
                        text: 'Создать пользователя',
                        style: GoogleFonts.raleway(
                            textStyle: TextStyle(
                                color: colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                height: 18.78/16
                            )
                        ),
                      )
                    ]
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
