import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:training_final_10/presentation/pages/log_in.dart';
import 'package:training_final_10/presentation/theme/colors.dart';
import 'package:training_final_10/presentation/widgets/CustomTextField.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool isConfirm = false;
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              const SizedBox(height: 66),
              Row(
                children: [
                  Container(
                      height: 44,
                      width: 44,
                      decoration: BoxDecoration(
                          color: colors.lightGray,
                          borderRadius: BorderRadius.circular(360)),
                      child: SvgPicture.asset('assets/back_arrow.svg',
                          fit: BoxFit.scaleDown)),
                ],
              ),
              const SizedBox(height: 11),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Регистрация',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 8),
                  Text(
                      'Заполните Свои Данные Или\n Продолжите Через Социальные Медиа',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleMedium)
                ],
              ),
              const SizedBox(height: 30),
              CustomTextField(
                  label: 'Ваше имя',
                  hint: 'xxxxxxxx',
                  controller: name,
                  onChanged: onChanged,
                  isValid: true
              ),
              const SizedBox(height: 12),
              CustomTextField(
                  label: 'Email',
                  hint: 'xyz@gmail.com',
                  controller: email,
                  onChanged: onChanged,
                  isValid: true
              ),
              const SizedBox(height: 30),
              CustomTextField(
                  label: 'Пароль',
                  hint: '••••••••',
                  controller: password,
                  enableObscure: true,
                  onChanged: onChanged,
                  isValid: true
              ),
              const SizedBox(height: 8,),
              Row(
                children: [
                  SizedBox(
                    height: 18,
                    width: 18,
                    child: Expanded(
                      child: Checkbox(
                          value: isConfirm,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)
                          ),
                          side: const BorderSide(color: Colors.transparent),
                          fillColor: MaterialStateProperty.all(colors.lightGray),
                          onChanged: (value){
                            setState(() {
                              isConfirm = value!;
                            });
                          }
                      ),
                    ),
                  ),
                  const SizedBox(width: 12,),
                  Text(
                    'Даю согласие на обработку\nперсональных данных',
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                          color: colors.gray,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          height: 18.78/16
                        )
                    ),
                  )
                ],
              ),
              const SizedBox(height: 12),
              SizedBox(
                width: double.infinity,
                height: 50,
                child: FilledButton(
                    onPressed: (){},
                    style: Theme.of(context).filledButtonTheme.style,
                    child: const Text('Зарегистрироватеься')
                ),
              ),
              const SizedBox(height: 111),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LogIn()));
                },
                child: RichText(text: TextSpan(
                  children: [
                    TextSpan(
                      text: 'Есть аккаунт? ',
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: colors.gray,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              height: 18.78/16
                          )
                      ),
                    ),
                    TextSpan(
                      text: 'Войти',
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              height: 18.78/16
                          )
                      ),
                    )
                  ]
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
