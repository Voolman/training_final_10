import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pinput/pinput.dart';
import 'package:training_final_10/domain/otp_verification_presenter.dart';
import 'package:training_final_10/presentation/theme/colors.dart';

class OTPVerification extends StatefulWidget {
  const OTPVerification({super.key});

  @override
  State<OTPVerification> createState() => _OTPVerificationState();
}

class _OTPVerificationState extends State<OTPVerification> {
  @override
  void initState(){
    super.initState();
    timer(
            (){
          setState(() {
          });
        }
    );
  }
  TextEditingController code = TextEditingController();
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              const SizedBox(height: 66),
              Row(
                children: [
                  Container(
                      height: 44,
                      width: 44,
                      decoration: BoxDecoration(
                          color: colors.lightGray,
                          borderRadius: BorderRadius.circular(360)),
                      child: SvgPicture.asset('assets/back_arrow.svg',
                          fit: BoxFit.scaleDown)),
                ],
              ),
              const SizedBox(height: 11),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'OTP проверка',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 8),
                  Text(
                      'Пожалуйста, Проверьте Свою\n Электронную Почту, Чтобы Увидеть\n Код Подтверждения',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleMedium)
                ],
              ),
              const SizedBox(height: 16),
              Row(
                children: [
                  SizedBox(width: 14,),
                  Text(
                    'OTP Код',
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: colors.black,
                            fontSize: 21,
                            fontWeight: FontWeight.w600,
                            height: 24.65/16
                        )
                    ),
                  )
                ],
              ),
              Pinput(
                controller: code,
                length: 6,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                defaultPinTheme: PinTheme(
                  height: 99,
                  width: 46,
                  textStyle: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          color: colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          height: 4/3
                      )
                  ),
                  decoration: BoxDecoration(
                    color: colors.lightGray,
                    border: Border.all(color: Colors.red, width: 1),
                    borderRadius: BorderRadius.circular(12)
                  )
                ),
                submittedPinTheme: PinTheme(
                    height: 99,
                    width: 46,
                    textStyle: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            height: 4/3
                        )
                    ),
                    decoration: BoxDecoration(
                        color: colors.lightGray,
                        border: Border.all(color: Colors.transparent),
                        borderRadius: BorderRadius.circular(12)
                    )
                ),
                focusedPinTheme: PinTheme(
                    height: 99,
                    width: 46,
                    textStyle: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            height: 4/3
                        )
                    ),
                    decoration: BoxDecoration(
                        color: colors.lightGray,
                        border: Border.all(color: Colors.transparent),
                        borderRadius: BorderRadius.circular(12)
                    )
                ),
              ),
              SizedBox(height: 22),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(width: 4,),
                      GestureDetector(
                        onTap: (){
                          (getSeconds() == 0) ? setState(() {
                            refresh(60);
                          }) : null;
                        },
                        child: Text(
                            'Отправить заново',
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  color: colors.gray,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  height: 14.09/12
                              )
                          ),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    '00:${getSeconds().toString()}',
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: colors.gray,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            height: 14.09/12
                        )
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
