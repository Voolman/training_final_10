import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_10/presentation/pages/otp_verification.dart';
import 'package:training_final_10/presentation/theme/colors.dart';
import 'package:training_final_10/presentation/widgets/CustomTextField.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController email = TextEditingController();
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              const SizedBox(height: 66),
              Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        height: 44,
                        width: 44,
                        decoration: BoxDecoration(
                            color: colors.lightGray,
                            borderRadius: BorderRadius.circular(360)),
                        child: SvgPicture.asset('assets/back_arrow.svg',
                            fit: BoxFit.scaleDown)),
                  ),
                ],
              ),
              const SizedBox(height: 11),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Забыл пароль',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 8),
                  Text(
                      'Введите Свою Учетную Запись\n Для Сброса',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.titleMedium)
                ],
              ),
              const SizedBox(height: 8),
              CustomTextField(
                  label: '',
                  hint: 'xyz@gmail.com',
                  controller: email,
                  onChanged: onChanged,
                  isValid: true
              ),
              const SizedBox(height: 40),
              SizedBox(
                width: double.infinity,
                height: 50,
                child: FilledButton(
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const OTPVerification()));
                    },
                    style: Theme.of(context).filledButtonTheme.style,
                    child: const Text('Отправить')
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
