import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:training_final_10/presentation/theme/colors.dart';

var colors = LightColorsApp();
var theme = ThemeData(
  textTheme: TextTheme(
    titleLarge: GoogleFonts.raleway(
        textStyle: TextStyle(
          color: Color.fromARGB(255, 43, 43, 43),
          fontWeight: FontWeight.w700,
          fontSize: 32,
          height: 37.57/32
        )
    ),
    titleMedium: GoogleFonts.poppins(
        textStyle: TextStyle(
            color: Color.fromARGB(255, 112, 123, 129),
            fontWeight: FontWeight.w400,
            fontSize: 16,
            height: 3/2
        )
    ),
    labelLarge: GoogleFonts.raleway(
        textStyle: TextStyle(
            color: Color.fromARGB(255, 43, 43, 43),
            fontWeight: FontWeight.w500,
            fontSize: 16,
            height: 5/4
        )
    ),
    titleSmall: GoogleFonts.poppins(
        textStyle: TextStyle(
            color: Color.fromARGB(255, 106, 106, 106),
            fontWeight: FontWeight.w500,
            fontSize: 14,
            height: 8/7
        )
    ),
  ),
    inputDecorationTheme: InputDecorationTheme(
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
            borderRadius: BorderRadius.circular(14),
        ),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.transparent),
            borderRadius: BorderRadius.circular(14),
        ),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: colors.red),
            borderRadius: BorderRadius.circular(14),
        )
    ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: colors.blue,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(13)
      ),
      padding: EdgeInsets.symmetric(vertical: 14, horizontal: 92),
      textStyle: GoogleFonts.raleway(
          textStyle: TextStyle(
              color: colors.white,
              fontWeight: FontWeight.w600,
              fontSize: 14,
              height: 11/7
          )
      )
    )
  )
);