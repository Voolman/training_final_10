import 'dart:async';

import '../data/storage/seconds.dart';

void time(){
  if (lostSeconds != 0){
    lostSeconds --;
  }
}

void timer(Function callback){
  Timer(
      const Duration(seconds: 1), (){
    time();
    callback();
    timer(callback);
  });
}

int getSeconds(){
  return lostSeconds;
}

void refresh(int num){
  lostSeconds = num;
}
